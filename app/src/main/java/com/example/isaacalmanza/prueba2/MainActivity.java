package com.example.isaacalmanza.prueba2;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView Cuerpoview;
    private Button mButton_abs;
    private Button mButton_pecho;
    private Button mButton_gluteos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Cuerpoview = (ImageView) findViewById(R.id.cuerpo);
        mButton_abs = (Button) findViewById(R.id.abs);
        mButton_pecho  = (Button) findViewById(R.id.pecho);
        mButton_gluteos = (Button) findViewById(R.id.gluteos);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Cuerpoview.setImageDrawable(getResources().getDrawable(R.drawable.cuerpos,getTheme()));
        }


        mButton_abs.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View v) {
               pintarAbs();

           }
       });
        mButton_pecho.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                pintarPecho();
            }
        });

        mButton_gluteos.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                pintarGluteos();
            }
        });


    }
    private void pintarAbs() {
        AnimatedVectorDrawableCompat animatedVector = AnimatedVectorDrawableCompat.create(getApplication(),R.drawable.animation_abs);

        Cuerpoview.setImageDrawable(animatedVector);
        if (animatedVector != null){
            animatedVector.mutate();
            animatedVector.start();
            Log.i("pintar abs","ha empezado");
        }

    }
    private void pintarPecho() {
        AnimatedVectorDrawableCompat animatedVector = AnimatedVectorDrawableCompat.create(getApplication(),R.drawable.animation_pecho);
        AnimatedVectorDrawableCompat vector = AnimatedVectorDrawableCompat.createFromXmlInner()
        Cuerpoview.setImageDrawable(animatedVector);
        if (animatedVector != null){
            animatedVector.start();
            Log.i("pintar pecho","ha empezado");
        }

    }
    private void pintarGluteos() {
        AnimatedVectorDrawableCompat animatedVector = AnimatedVectorDrawableCompat.create(getApplication(),R.drawable.animation_gluteos);

        Cuerpoview.setImageDrawable(animatedVector);
        if (animatedVector != null){
            animatedVector.start();
            Log.i("pintar gluteos","ha empezado");
        }

    }


}
